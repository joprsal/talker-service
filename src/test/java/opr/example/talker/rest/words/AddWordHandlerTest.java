package opr.example.talker.rest.words;

import static opr.example.talker.words.Adjective.adjective;
import static opr.example.talker.words.Noun.noun;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;

import opr.example.talker.words.Word;
import opr.example.talker.words.WordService;


public class AddWordHandlerTest {

    @Mock
    private WordService wordService;
    private AddWordHandler addWordHandler;
    private String requestBody;
    private String wordInPath;
    
    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        addWordHandler = new AddWordHandler(wordService);
    }

    
    @Test
    public void should_return_status_201_when_created() throws Exception {
        givenWordInPath("apple");
        givenWordInStore(false);
        givenRequestBody("{ \"word\": { \"wordCategory\": \"NOUN\" } }");
        assertWordAdded(201, noun("apple"));
    }
    
    @Test
    public void should_return_status_200_when_already_exists() throws Exception {
        givenWordInPath("green");
        givenWordInStore(true);
        givenRequestBody("{ \"word\": { \"wordCategory\": \"ADJECTIVE\" } }");
        assertWordAdded(200, adjective("green"));
    }

    
    private void givenWordInPath(String wordText) {
        this.wordInPath = wordText;
    }
    
    private void givenWordInStore(boolean isWordAlreadyInStore) {
        when(wordService.addWord(Mockito.any(Word.class))).thenReturn(!isWordAlreadyInStore);
    }
    
    private void givenRequestBody(String bodyContent) {
        this.requestBody = bodyContent;
    }

    private void assertWordAdded(int expectedStatus, Word expectedWordAdded) throws Exception {
        JsonParser bodyParser = new JsonFactory().createParser(requestBody);
        AtomicInteger statusCodeHolder = new AtomicInteger();
        
        addWordHandler.handle(wordInPath, bodyParser, statusCodeHolder);
        
        assertThat(statusCodeHolder.get(), is(expectedStatus));
        assertAddedWord(expectedWordAdded);
    }

    private void assertAddedWord(Word expectedWordAdded) {
        ArgumentCaptor<Word> addedWord = ArgumentCaptor.forClass(Word.class);
        verify(wordService).addWord(addedWord.capture());
        assertThat(addedWord.getValue(), is(expectedWordAdded));
    }
}
