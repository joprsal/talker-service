package opr.example.talker.rest.words;

import static net.javacrumbs.jsonunit.fluent.JsonFluentAssert.assertThatJson;
import static opr.example.talker.rest.words.JsonStringGenerator.generateJson;
import static opr.example.talker.words.Noun.noun;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import opr.example.talker.words.Word;
import opr.example.talker.words.WordService;


public class GetWordHandlerTest {

    @Mock
    private WordService wordService;
    private GetWordHandler getWordHandler;
    private String searchWord;
    
    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        getWordHandler = new GetWordHandler(wordService);
    }

    
    @Test
    public void should_format_word_when_found() throws Exception {
        givenWordFound(noun("apple"));
        assertGeneratedJson("{ \"word\": {\"word\": \"apple\", \"wordCategory\": \"NOUN\"} }");
    }
    
    @Test
    public void should_format_null_when_not_found() throws Exception {
        givenWordNotFound();
        assertGeneratedJson("{ \"word\": null }");
    }

    
    private void givenWordFound(Word word) {
        this.searchWord = word.getText();
        when(wordService.getWord(searchWord)).thenReturn(Optional.of(word));
    }
    
    private void givenWordNotFound() {
        this.searchWord = "anything";
        when(wordService.getWord(searchWord)).thenReturn(Optional.empty());
    }

    private void assertGeneratedJson(String expectedJson) throws Exception {
        String json = generateJson(gen -> getWordHandler.handle(searchWord, gen)).asString();
        assertThatJson(json).isEqualTo(expectedJson);
    }
}
