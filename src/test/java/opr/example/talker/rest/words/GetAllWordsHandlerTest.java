package opr.example.talker.rest.words;

import static net.javacrumbs.jsonunit.fluent.JsonFluentAssert.assertThatJson;
import static opr.example.talker.rest.words.JsonStringGenerator.generateJson;
import static opr.example.talker.words.Adjective.adjective;
import static opr.example.talker.words.Noun.noun;
import static opr.example.talker.words.Verb.verb;
import static org.mockito.Mockito.when;

import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import opr.example.talker.words.Word;
import opr.example.talker.words.WordService;


public class GetAllWordsHandlerTest {

    @Mock
    private WordService wordService;
    private GetAllWordsHandler getAllWordsHandler;
    
    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        getAllWordsHandler = new GetAllWordsHandler(wordService);
    }

    
    @Test
    public void should_generate_empty_array_when_no_words_exist() throws Exception {
        givenAllWords();
        assertGeneratedJson("[]");
    }
    
    @Test
    public void should_generate_array_with_all_words_in_alphabetical_order() throws Exception {
        givenAllWords(noun("apple"), verb("is"), adjective("green"));
        assertGeneratedJson("[\"apple\", \"is\", \"green\"]");
    }

    
    private void givenAllWords(Word... words) {
        when(wordService.getAllWords()).thenReturn(Stream.of(words));
    }

    private void assertGeneratedJson(String expectedJson) throws Exception {
        String json = generateJson(gen -> getAllWordsHandler.handle(gen)).asString();
        assertThatJson(json).isEqualTo(expectedJson);
    }
}
