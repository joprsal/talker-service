package opr.example.talker.rest.words;

import static net.javacrumbs.jsonunit.fluent.JsonFluentAssert.assertThatJson;
import static opr.example.talker.words.Adjective.adjective;
import static opr.example.talker.words.Noun.noun;
import static opr.example.talker.words.Verb.verb;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyString;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import opr.example.talker.words.Word;
import opr.example.talker.words.WordService;
import ratpack.func.Action;
import ratpack.handling.Chain;
import ratpack.http.client.ReceivedResponse;
import ratpack.test.embed.EmbeddedApp;

public class WordsHandlerTest {

	@Mock
	private Chain handlingChain;
	@Mock
	private WordService wordService;

	private WordsHandler handler;
	private EmbeddedApp testApp;


	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		handler = new WordsHandler(wordService);
		testApp = EmbeddedApp.fromHandlers(chain -> handler.registerWith(chain));
	}

	
    @Test
    @SuppressWarnings("unchecked")
	public void should_register_with_handling_chain() throws Exception {
		handler.registerWith(handlingChain);

		verify(handlingChain).prefix(eq("words"), any(Action.class));
	}

    @Test
    public void should_delegate_to_getAllWords() throws Exception {
        when(wordService.getAllWords()).thenReturn(Stream.of(noun("apple"), verb("is"), adjective("green")));
        
        testApp.test(httpClient -> {
            assertThatJson(httpClient.getText("words"))
                    .isEqualTo("[\"apple\", \"is\", \"green\"]");
        });
        verify(wordService).getAllWords();
    }

    @Test
    public void should_delegate_to_getWord() throws Exception {
        final String WORD_TEXT = "apple";
        final Word WORD = noun(WORD_TEXT);
        
        when(wordService.getWord(WORD_TEXT)).thenReturn(Optional.of(WORD));
        
        testApp.test(httpClient -> {
            assertThatJson(httpClient.getText("words/apple"))
                    .isEqualTo("{ \"word\": { \"word\": \"apple\", \"wordCategory\": \"NOUN\" } }");
        });
        verify(wordService).getWord(WORD_TEXT);
    }

    @Test
    public void should_delegate_to_addWord() throws Exception {
        final String WORD_TEXT = "green";
        final Word WORD = adjective(WORD_TEXT);
        
        when(wordService.addWord(WORD)).thenReturn(true);
        
        testApp.test(httpClient -> {
            ReceivedResponse resp = httpClient.requestSpec(reqSpec -> {
                reqSpec.getBody().text("{ \"word\": { \"wordCategory\": \"ADJECTIVE\" } }");
            }).put("words/green");
            assertThat(resp.getBody().getText(), isEmptyString());
            assertThat(resp.getStatusCode(), is(201));
            assertThat(resp.getHeaders().get("Location"), is("/words/green"));
        });
        verify(wordService).addWord(WORD);
    }
}
