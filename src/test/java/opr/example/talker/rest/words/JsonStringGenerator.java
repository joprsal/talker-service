package opr.example.talker.rest.words;

import java.io.ByteArrayOutputStream;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;


public class JsonStringGenerator {

    private final JsonGenerateLogic jsonGenerateLogic;
    
    private JsonStringGenerator(JsonGenerateLogic jsonGenerateLogic) {
        this.jsonGenerateLogic = jsonGenerateLogic;
    }
    
    public static JsonStringGenerator generateJson(JsonGenerateLogic jsonGenerateLogic) {
        return new JsonStringGenerator(jsonGenerateLogic);
    }

    public String asString() throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (JsonGenerator jsonGenerator = new JsonFactory().createGenerator(baos)) {
            jsonGenerateLogic.generateJson(jsonGenerator);
        }
        String generatedJson = baos.toString();
        return generatedJson;
    }
    
    public interface JsonGenerateLogic {
        void generateJson(JsonGenerator jsonGenerator) throws Exception;
    }
}
