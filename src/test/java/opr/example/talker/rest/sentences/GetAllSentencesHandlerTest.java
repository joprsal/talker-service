package opr.example.talker.rest.sentences;

import static net.javacrumbs.jsonunit.fluent.JsonFluentAssert.assertThatJson;
import static opr.example.talker.rest.words.JsonStringGenerator.generateJson;
import static opr.example.talker.words.Adjective.adjective;
import static opr.example.talker.words.Noun.noun;
import static opr.example.talker.words.Verb.verb;
import static org.mockito.Mockito.when;

import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import opr.example.talker.sentences.Sentence;
import opr.example.talker.sentences.SentenceService;


public class GetAllSentencesHandlerTest {

    @Mock
    private SentenceService sentenceService;
    private GetAllSentencesHandler getAllSentencesHandler;
    
    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        getAllSentencesHandler = new GetAllSentencesHandler(sentenceService);
    }

    
    @Test
    public void should_generate_empty_array_when_no_sentences_exist() throws Exception {
        givenAllSentences();
        assertGeneratedJson("[]");
    }
    
    @Test
    public void should_generate_array_with_all_words_in_alphabetical_order() throws Exception {
        givenAllSentences(
                new Sentence(noun("apple"), verb("is"), adjective("green")),
                new Sentence(noun("snow"), verb("is"), adjective("white")));
        assertGeneratedJson("[\"apple is green\", \"snow is white\"]");
    }

    
    private void givenAllSentences(Sentence... sentences) {
        when(sentenceService.getAllSentences()).thenReturn(Stream.of(sentences));
    }

    private void assertGeneratedJson(String expectedJson) throws Exception {
        String json = generateJson(gen -> getAllSentencesHandler.handle(gen)).asString();
        assertThatJson(json).isEqualTo(expectedJson);
    }
}
