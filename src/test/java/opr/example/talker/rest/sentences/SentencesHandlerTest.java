package opr.example.talker.rest.sentences;

import static java.util.Arrays.asList;
import static net.javacrumbs.jsonunit.core.Option.IGNORING_EXTRA_FIELDS;
import static net.javacrumbs.jsonunit.fluent.JsonFluentAssert.assertThatJson;
import static opr.example.talker.words.Adjective.adjective;
import static opr.example.talker.words.Noun.noun;
import static opr.example.talker.words.Verb.verb;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyString;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.Optional;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import opr.example.talker.sentences.Sentence;
import opr.example.talker.sentences.SentenceService;
import opr.example.talker.sentences.SentenceWithInfo;
import ratpack.func.Action;
import ratpack.handling.Chain;
import ratpack.http.client.ReceivedResponse;
import ratpack.test.embed.EmbeddedApp;

public class SentencesHandlerTest {

	@Mock
	private Chain handlingChain;
    @Mock
    private SentenceService sentenceService;

	private SentencesHandler handler;
	private EmbeddedApp testApp;


	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		handler = new SentencesHandler(sentenceService);
		testApp = EmbeddedApp.fromHandlers(chain -> handler.registerWith(chain));
	}

	
    @Test
    @SuppressWarnings("unchecked")
	public void should_register_with_handling_chain() throws Exception {
		handler.registerWith(handlingChain);

		verify(handlingChain).prefix(eq("sentences"), any(Action.class));
	}

    @Test
    public void should_delegate_to_getAllSentences() throws Exception {
        when(sentenceService.getAllSentences()).thenReturn(Stream.of(
                new Sentence(noun("apple"), verb("is"), adjective("green")),
                new Sentence(noun("apple"), verb("is"), adjective("red")),
                new Sentence(noun("pear"), verb("is"), adjective("green"))));
        
        testApp.test(httpClient -> {
            assertThatJson(httpClient.getText("sentences"))
                    .isEqualTo("[\"apple is green\", \"apple is red\", \"pear is green\"]");
        });
        verify(sentenceService).getAllSentences();
    }

    @Test
    public void should_delegate_to_getSentence() throws Exception {
        final long SENTENCE_ID = 123L;
        final SentenceWithInfo SENTENCE = new SentenceWithInfo(
                new Sentence(noun("snow"), verb("is"), adjective("white")), 33L);
        
        when(sentenceService.getSentence(SENTENCE_ID)).thenReturn(Optional.of(SENTENCE));
        
        testApp.test(httpClient -> {
            assertThatJson(httpClient.getText("sentences/123"))
                    .when(IGNORING_EXTRA_FIELDS)
                    .isEqualTo("{\"sentence\": { "
                            + "\"text\": \"snow is white\", "
                            + "\"sameSentenceGroupId\": 33 } }");
        });
        verify(sentenceService).getSentence(SENTENCE_ID);
    }

    @Test
    public void should_delegate_to_generateSentence() throws Exception {
        final long SENTENCE_ID = 123L;
        
        when(sentenceService.generateSentence()).thenReturn(SENTENCE_ID);
        
        testApp.test(httpClient -> {
            ReceivedResponse resp = httpClient.post("sentences/generate");
            assertThat(resp.getBody().getText(), isEmptyString());
            assertThat(resp.getStatusCode(), is(201));
            assertThat(resp.getHeaders().get("Location"), is("/sentences/123"));
        });
    }

    @Test
    public void should_delegate_to_getSameSentenceGroup() throws Exception {
        final long SAME_SENTENCE_GROUP_ID = 123L;
        final long SENTENCE_ID1 = 101L;
        final long SENTENCE_ID2 = 102L;
        final SentenceWithInfo SENTENCE = new SentenceWithInfo(
                new Sentence(noun("snow"), verb("is"), adjective("white")),
                SAME_SENTENCE_GROUP_ID);
        
        when(sentenceService.getSentence(SENTENCE_ID1)).thenReturn(Optional.of(SENTENCE));
        when(sentenceService.getSentence(SENTENCE_ID2)).thenReturn(Optional.of(SENTENCE));
        when(sentenceService.getSameSentenceGroup(SAME_SENTENCE_GROUP_ID))
                .thenReturn(Optional.of(new HashSet<>(asList(SENTENCE_ID1, SENTENCE_ID2))));
        
        testApp.test(httpClient -> {
            assertThatJson(httpClient.getText("sentences/sameSentenceGroup/" + SAME_SENTENCE_GROUP_ID))
                    .isEqualTo("{\"sameSentenceGroup\": { "
                            + "\"text\": \"snow is white\", "
                            + "\"count\": 2, "
                            + "\"sentenceIds\": [" + SENTENCE_ID1 + ", " + SENTENCE_ID2 + "] } }");
        });
        verify(sentenceService).getSameSentenceGroup(SAME_SENTENCE_GROUP_ID);
    }

}
