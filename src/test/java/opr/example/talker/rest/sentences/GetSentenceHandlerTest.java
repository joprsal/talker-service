package opr.example.talker.rest.sentences;

import static net.javacrumbs.jsonunit.core.Option.IGNORING_EXTRA_FIELDS;
import static net.javacrumbs.jsonunit.core.Option.IGNORING_VALUES;
import static net.javacrumbs.jsonunit.fluent.JsonFluentAssert.assertThatJson;
import static opr.example.talker.rest.words.JsonStringGenerator.generateJson;
import static opr.example.talker.words.Adjective.adjective;
import static opr.example.talker.words.Noun.noun;
import static opr.example.talker.words.Verb.verb;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import net.javacrumbs.jsonunit.core.Option;
import opr.example.talker.sentences.Sentence;
import opr.example.talker.sentences.SentenceFormatter;
import opr.example.talker.sentences.SentenceService;
import opr.example.talker.sentences.SentenceWithInfo;


public class GetSentenceHandlerTest {

    @Mock
    private SentenceService sentenceService;
    private GetSentenceHandler getSentenceHandler;
    private long sentenceId = -1L;
    private SentenceFormatter sentenceFormatter = SentenceFormatter.DEFAULT;
    
    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        when(sentenceService.getSentence(-1L)).thenReturn(Optional.empty());
        getSentenceHandler = new GetSentenceHandler(sentenceService);
    }

    
    @Test
    public void should_format_default_sentence_when_found() throws Exception {
        givenStoredSentence(new Sentence(noun("apple"), verb("is"), adjective("green")));
        assertGeneratedJson("{ \"sentence\": { \"text\": \"apple is green\" } }");
    }
    
    @Test
    public void should_format_yoda_sentence_when_found() throws Exception {
        givenStoredSentence(new Sentence(noun("apple"), verb("is"), adjective("green")));
        givenYodaTalk();
        assertGeneratedJson("{ \"sentence\": { \"text\": \"green apple is\" } }");
    }

    @Test
    public void should_format_null_when_not_found() throws Exception {
        assertGeneratedJson("{ \"sentence\": null }");
    }
    
    @Test
    public void should_include_sameSentenceGroupId() throws Exception {
        givenStoredSentence(new Sentence(noun("apple"), verb("is"), adjective("green")));
        assertGeneratedJson("{ \"sentence\": { \"sameSentenceGroupId\": 1 } }", IGNORING_VALUES);
    }
    
    @Test
    public void should_return_sentence_display_count() throws Exception {
        givenStoredSentence(new Sentence(noun("apple"), verb("is"), adjective("green")));
        assertGeneratedJson("{ \"sentence\": { "
                + "\"text\": \"apple is green\", "
                + "\"viewDisplayCount\": 1 } }");
        assertGeneratedJson("{ \"sentence\": { "
                + "\"text\": \"apple is green\", "
                + "\"viewDisplayCount\": 2 } }");
    }
    
    
    private void givenYodaTalk() {
        this.sentenceFormatter = SentenceFormatter.YODA_TALK;
    }
    
    private void givenStoredSentence(Sentence sentence) {
        this.sentenceId = 123L;
        SentenceWithInfo sentenceWithInfo = new SentenceWithInfo(sentence, 1L);
        when(sentenceService.getSentence(123L)).thenReturn(Optional.of(sentenceWithInfo));
        when(sentenceService.getSentence(123L)).thenReturn(Optional.of(sentenceWithInfo));
    }
    
    private void assertGeneratedJson(String expectedJson, Option... assertOptions) throws Exception {
        String json = generateJson(gen -> getSentenceHandler.handle(sentenceId, sentenceFormatter, gen)).asString();
        assertThatJson(json).when(IGNORING_EXTRA_FIELDS, assertOptions).isEqualTo(expectedJson);
    }
}
