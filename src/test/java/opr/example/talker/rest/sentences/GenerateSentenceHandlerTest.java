package opr.example.talker.rest.sentences;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.util.concurrent.atomic.AtomicLong;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import opr.example.talker.sentences.SentenceService;


public class GenerateSentenceHandlerTest {

    @Mock
    private SentenceService sentenceService;
    private GenerateSentenceHandler generateSentenceHandler;
    private long sentenceIdInResp;
    
    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        generateSentenceHandler = new GenerateSentenceHandler(sentenceService);
    }

    
    @Test
    public void should_provide_generated_sentence_id() throws Exception {
        givenNextSentenceId(123L);
        generateSentence();
        assertThat(sentenceIdInResp, is(123L));
    }
    
    
    private void givenNextSentenceId(long nextId) {
        when(sentenceService.generateSentence()).thenReturn(nextId);
    }
    
    private void generateSentence() throws Exception {
        AtomicLong sentenceIdHolder = new AtomicLong();
        generateSentenceHandler.handle(sentenceIdHolder);
        this.sentenceIdInResp = sentenceIdHolder.get();
    }
}
