package opr.example.talker.rest.sentences;

import static java.util.stream.Collectors.toSet;
import static net.javacrumbs.jsonunit.fluent.JsonFluentAssert.assertThatJson;
import static opr.example.talker.rest.words.JsonStringGenerator.generateJson;
import static opr.example.talker.words.Adjective.adjective;
import static opr.example.talker.words.Noun.noun;
import static opr.example.talker.words.Verb.verb;
import static org.mockito.Mockito.when;

import java.util.Optional;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import opr.example.talker.sentences.Sentence;
import opr.example.talker.sentences.SentenceService;
import opr.example.talker.sentences.SentenceWithInfo;


public class GetSameSentenceGroupHandlerTest {

    @Mock
    private SentenceService sentenceService;
    private GetSameSentenceGroupHandler getSameSentenceGroupHandler;
    private long sameSentenceGroupId = -1L;
    
    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        when(sentenceService.getSameSentenceGroup(-1L)).thenReturn(Optional.empty());
        getSameSentenceGroupHandler = new GetSameSentenceGroupHandler(sentenceService);
    }

    
    @Test
    public void should_format_same_sentence_group_when_found() throws Exception {
        givenSameSentenceGroup(123L,
                new Sentence(noun("apple"), verb("is"), adjective("green")),
                101L, 102L);
        
        assertGeneratedJson("{ \"sameSentenceGroup\": { "
                + "\"text\": \"apple is green\", "
                + "\"count\": 2, "
                + "\"sentenceIds\": [101, 102] } }");
    }

    @Test
    public void should_format_null_when_not_found() throws Exception {
        assertGeneratedJson("{ \"sameSentenceGroup\": null }");
    }
    
    
    private void givenSameSentenceGroup(long groupId, Sentence sentence, Long... sentenceIds) {
        this.sameSentenceGroupId = groupId;
        for (long sentenceId : sentenceIds) {
            SentenceWithInfo sentenceWithInfo = new SentenceWithInfo(sentence, groupId);
            when(sentenceService.getSentence(sentenceId)).thenReturn(Optional.of(sentenceWithInfo));
        }
        when(sentenceService.getSameSentenceGroup(groupId))
                .thenReturn(Optional.of(Stream.of(sentenceIds).collect(toSet())));
    }
    
    private void assertGeneratedJson(String expectedJson) throws Exception {
        String json = generateJson(gen -> getSameSentenceGroupHandler.handle(sameSentenceGroupId, gen)).asString();
        assertThatJson(json).isEqualTo(expectedJson);
    }
}
