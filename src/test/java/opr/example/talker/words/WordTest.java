package opr.example.talker.words;

import static opr.example.talker.words.WordCategory.NOUN;
import static opr.example.talker.words.WordCategory.VERB;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.HashSet;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class WordTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    
    
    @Test
    public void should_return_correct_text() {
        assertThat(dummyWord("foo").getText(), is(equalTo("foo")));
    }
    
    
    @Test
    public void should_fail_to_construct_when_text_is_null() {
        expectedException.expect(NullPointerException.class);
        dummyWord(null);
    }
    
    @Test
    public void should_fail_to_construct_when_text_is_blank() {
        expectedException.expect(IllegalArgumentException.class);
        dummyWord(" ");
    }
    
    @Test
    public void should_store_all_different_words_in_hash_set() {
        HashSet<Word> words = new HashSet<>();
        words.add(dummyWord("apple", NOUN));
        words.add(dummyWord("move", NOUN));
        words.add(dummyWord("move", VERB));
        
        assertThat(words.size(), is(3));
    }

    @Test
    public void should_store_one_instance_of_same_word_in_hash_set() {
        HashSet<Word> words = new HashSet<>();
        words.add(dummyWord("apple", NOUN));
        words.add(dummyWord("apple", NOUN));
        
        assertThat(words.size(), is(1));
    }

    
    private static Word dummyWord(String text) {
        return dummyWord(text, WordCategory.NOUN);
    }
    private static Word dummyWord(String text, WordCategory category) {
        return new Word(text) {
            
            @Override
            public WordCategory getCategory() {
                return category;
            }

            @Override
            public <T> T byCategory(
                    LogicForCategory<Noun, T> logicForNoun,
                    LogicForCategory<Verb, T> logicForVerb,
                    LogicForCategory<Adjective, T> logicForAdjective) {
                throw new UnsupportedOperationException();
            }
        };
    }
    
}
