package opr.example.talker.words;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;
import static opr.example.talker.words.Adjective.adjective;
import static opr.example.talker.words.Noun.noun;
import static opr.example.talker.words.Verb.verb;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Stream;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class WordServiceTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private Set<String> blacklistedWords = new HashSet<>();
    private WordService wordService = new WordService(blacklistedWords);
    
    @Test
    public void should_get_empty_all_words_when_nothing_was_added() {
        assertThat(wordService.getAllWords().count(), is(0L));
    }
    
    @Test
    public void should_get_no_word_when_nothing_was_added() {
        assertThat(wordService.getWord("apple").isPresent(), is(false));
    }
    
    @Test
    public void should_get_all_added_words_in_alphabetical_order() {
        wordService.addWord(noun("apple"));
        wordService.addWord(verb("is"));
        wordService.addWord(adjective("green"));

        assertStreamContents(
                wordService.getAllWords(),
                noun("apple"), adjective("green"), verb("is"));
    }

    @Test
    public void should_get_one_word_when_previously_added() {
        wordService.addWord(noun("apple"));
        wordService.addWord(verb("is"));
        wordService.addWord(adjective("green"));

        assertThat(wordService.getWord("is").get(), is(equalTo(verb("is"))));
    }

    @Test
    public void should_get_empty_random_words_when_nothing_was_added() {
        assertThat(wordService.getRandomNoun(), is(Optional.empty()));
        assertThat(wordService.getRandomVerb(), is(Optional.empty()));
        assertThat(wordService.getRandomAdjective(), is(Optional.empty()));
    }

    @Test
    public void should_get_random_noun() {
        setupThreeWordsOfEachType();

        assertThat(wordService.getRandomNoun().get().getText(), startsWith("noun"));
        testUniformDistributionOf(words -> words.getRandomNoun(), 3, 10000);
    }

    @Test
    public void should_get_random_verb() {
        setupThreeWordsOfEachType();

        assertThat(wordService.getRandomVerb().get().getText(), startsWith("verb"));
        testUniformDistributionOf(words -> words.getRandomVerb(), 3, 10000);
    }

    @Test
    public void should_get_random_adjective() {
        setupThreeWordsOfEachType();

        assertThat(wordService.getRandomAdjective().get().getText(), startsWith("adjective"));
        testUniformDistributionOf(words -> words.getRandomAdjective(), 3, 10000);
    }

    private void testUniformDistributionOf(
            Function<WordService, Optional<? extends Word>> fetchRandomLogic,
            int numberOfUniqueValuesPossible,
            int numberOfRandomPicks) {
        
        Map<Word, Long> wordCounts =
                Stream.generate(() -> fetchRandomLogic.apply(wordService).get())
                        .limit(numberOfRandomPicks)
                        .map(Word.class::cast)
                        .collect(groupingBy(identity(), counting()));
        
        assertThat(wordCounts.size(), is(lessThanOrEqualTo(numberOfUniqueValuesPossible)));
        double expectedMinCount = 0.8 * numberOfRandomPicks / numberOfUniqueValuesPossible;
        double expectedMaxCount = 1.2 * numberOfRandomPicks / numberOfUniqueValuesPossible;
        for (Entry<Word, Long> entry : wordCounts.entrySet()) {
            assertThat("Word counts should have uniform distribution: " + wordCounts,
                    (double) entry.getValue(),
                    is(allOf(greaterThan(expectedMinCount), lessThan(expectedMaxCount))));
        }
    }
    
    @Test
    public void should_reject_words_on_blacklist() {
        final String BAD_WORD = "green";
        blacklistedWords.add(BAD_WORD);
        
        wordService.addWord(noun("apple"));
        
        expectedException.expect(IllegalInputException.class);
        expectedException.expectMessage("not allowed");
        expectedException.expectMessage(BAD_WORD);
        
        wordService.addWord(adjective(BAD_WORD));
    }
    

    private void setupThreeWordsOfEachType() {
        wordService.addWord(noun("noun1"));
        wordService.addWord(noun("noun2"));
        wordService.addWord(verb("verb1"));
        wordService.addWord(adjective("adjective1"));
        wordService.addWord(verb("verb2"));
        wordService.addWord(verb("verb3"));
        wordService.addWord(adjective("adjective2"));
        wordService.addWord(adjective("adjective3"));
        wordService.addWord(noun("noun3"));
    }

    @SuppressWarnings("unchecked")
    private <T> void assertStreamContents(Stream<T> actualStream, T... expectedContents) {
        assertThat(actualStream.collect(toList()), is(Arrays.asList(expectedContents)));
    }
}
