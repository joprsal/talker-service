package opr.example.talker.words;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class AdjectiveTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    
    
    @Test
    public void should_return_correct_text() {
        assertThat(Adjective.adjective("green").getText(), is(equalTo("green")));
    }
    
    @Test
    public void should_report_correct_category() {
        assertThat(Adjective.adjective("green").getCategory(), is(equalTo(WordCategory.ADJECTIVE)));
    }
    
    @Test
    public void should_fail_to_construct_when_text_is_null() {
        expectedException.expect(NullPointerException.class);
        Adjective.adjective(null);
    }
    
    @Test
    public void should_fail_to_construct_when_text_is_blank() {
        expectedException.expect(IllegalArgumentException.class);
        Adjective.adjective(" ");
    }

    @Test
    public void should_call_correct_byCategory_logic() {
        String ret = Adjective.adjective("green").byCategory(
                noun -> "noun",
                verb -> "verb",
                adjective -> "adjective");
        assertThat(ret, is("adjective"));
    }
}
