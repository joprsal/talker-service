package opr.example.talker.words;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class NounTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    
    
    @Test
    public void should_return_correct_text() {
        assertThat(Noun.noun("apple").getText(), is(equalTo("apple")));
    }
    
    @Test
    public void should_report_correct_category() {
        assertThat(Noun.noun("apple").getCategory(), is(equalTo(WordCategory.NOUN)));
    }
    
    @Test
    public void should_fail_to_construct_when_text_is_null() {
        expectedException.expect(NullPointerException.class);
        Noun.noun(null);
    }
    
    @Test
    public void should_fail_to_construct_when_text_is_blank() {
        expectedException.expect(IllegalArgumentException.class);
        Noun.noun(" ");
    }

    @Test
    public void should_call_correct_byCategory_logic() {
        String ret = Noun.noun("apple").byCategory(
                noun -> "noun",
                verb -> "verb",
                adjective -> "adjective");
        assertThat(ret, is("noun"));
    }
}
