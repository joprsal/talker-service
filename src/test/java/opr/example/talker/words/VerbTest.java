package opr.example.talker.words;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class VerbTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    
    
    @Test
    public void should_return_correct_text() {
        assertThat(Verb.verb("is").getText(), is(equalTo("is")));
    }
    
    @Test
    public void should_report_correct_category() {
        assertThat(Verb.verb("is").getCategory(), is(equalTo(WordCategory.VERB)));
    }
    
    @Test
    public void should_fail_to_construct_when_text_is_null() {
        expectedException.expect(NullPointerException.class);
        Verb.verb(null);
    }
    
    @Test
    public void should_fail_to_construct_when_text_is_blank() {
        expectedException.expect(IllegalArgumentException.class);
        Verb.verb(" ");
    }

    @Test
    public void should_call_correct_byCategory_logic() {
        String ret = Verb.verb("is").byCategory(
                noun -> "noun",
                verb -> "verb",
                adjective -> "adjective");
        assertThat(ret, is("verb"));
    }
}
