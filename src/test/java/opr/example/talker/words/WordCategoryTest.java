package opr.example.talker.words;

import static opr.example.talker.words.WordCategory.ADJECTIVE;
import static opr.example.talker.words.WordCategory.NOUN;
import static opr.example.talker.words.WordCategory.VERB;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class WordCategoryTest {

    
    @Test
    public void should_create_word_of_category_noun() {
        assertThat(NOUN.newWordOfThis("apple"), is(instanceOf(Noun.class)));
    }
    
    @Test
    public void should_create_word_of_category_verb() {
        assertThat(VERB.newWordOfThis("is"), is(instanceOf(Verb.class)));
    }
    
    @Test
    public void should_create_word_of_category_adjective() {
        assertThat(ADJECTIVE.newWordOfThis("green"), is(instanceOf(Adjective.class)));
    }
    
}
