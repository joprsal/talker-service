package opr.example.talker.sentences;

import static opr.example.talker.words.Adjective.adjective;
import static opr.example.talker.words.Noun.noun;
import static opr.example.talker.words.Verb.verb;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.HashSet;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class SentenceTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    
    
    @Test
    public void should_build_when_required_word_categories_provided() {
        Sentence sentence = new Sentence(noun("apple"), verb("is"), adjective("green"));
        
        assertThat(sentence.getNoun(), is(noun("apple")));
        assertThat(sentence.getVerb(), is(verb("is")));
        assertThat(sentence.getAdjective(), is(adjective("green")));
    }

    @Test
    public void should_fail_to_construct_when_any_noun_is_null() {
        expectedException.expect(NullPointerException.class);
        new Sentence(null, verb("is"), adjective("green"));
    }

    @Test
    public void should_fail_to_construct_when_any_verb_is_null() {
        expectedException.expect(NullPointerException.class);
        new Sentence(noun("apple"), null, adjective("green"));
    }

    @Test
    public void should_fail_to_construct_when_any_adjective_is_null() {
        expectedException.expect(NullPointerException.class);
        new Sentence(noun("apple"), verb("is"), null);
    }
    
    
    @Test
    public void should_store_different_sentences_in_hash_set() {
        HashSet<Sentence> set = new HashSet<>();
        set.add(new Sentence(noun("apple"), verb("is"), adjective("green")));
        set.add(new Sentence(noun("apple"), verb("is"), adjective("red")));
        
        assertThat(set.size(), is(2));
    }

    @Test
    public void should_store_one_instance_of_same_sentence_in_hash_set() {
        HashSet<Sentence> set = new HashSet<>();
        set.add(new Sentence(noun("apple"), verb("is"), adjective("green")));
        set.add(new Sentence(noun("apple"), verb("is"), adjective("green")));

        assertThat(set.size(), is(1));
    }
}
