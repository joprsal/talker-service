package opr.example.talker.sentences;

import static opr.example.talker.words.Adjective.adjective;
import static opr.example.talker.words.Noun.noun;
import static opr.example.talker.words.Verb.verb;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class SentenceWithInfoTest {

    private static final Sentence TEST_SENTENCE = new Sentence(noun("apple"), verb("is"), adjective("green"));
    private static final long TEST_SAME_SENTENCE_GROUP_ID = 12L;
    
    @Test
    public void should_provide_associated_sentence() {
        SentenceWithInfo s = new SentenceWithInfo(TEST_SENTENCE, TEST_SAME_SENTENCE_GROUP_ID);
        assertThat(s.getSentence(), is(TEST_SENTENCE));
    }
    
    @Test
    public void should_provide_associated_groupId() {
        SentenceWithInfo s = new SentenceWithInfo(TEST_SENTENCE, TEST_SAME_SENTENCE_GROUP_ID);
        assertThat(s.getGroupId(), is(TEST_SAME_SENTENCE_GROUP_ID));
    }
}
