package opr.example.talker.sentences;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toSet;
import static opr.example.talker.words.Adjective.adjective;
import static opr.example.talker.words.Noun.noun;
import static opr.example.talker.words.Verb.verb;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.Optional;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.google.common.collect.ImmutableSet;

import opr.example.talker.words.Adjective;
import opr.example.talker.words.Noun;
import opr.example.talker.words.Verb;
import opr.example.talker.words.WordService;

public class SentenceServiceTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    @Mock
    private WordService wordService;
    private SentenceService sentenceService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        sentenceService = new SentenceService(wordService);
    }
    
    @Test
    public void should_get_empty_all_sentences_when_nothing_was_added() {
        assertThat(sentenceService.getAllSentences().count(), is(0L));
    }
    
    @Test
    public void should_get_no_sentence_when_id_not_available() {
        assertThat(sentenceService.getSentence(666L), is(Optional.empty()));
    }
    
    @Test
    public void should_fail_to_generate_sentence_when_not_enough_words() {
        givenRandomWords(noun("apple"), verb("is"), null);
        
        expectedException.expect(IllegalStateException.class);
        
        sentenceService.generateSentence();
    }
    
    @Test
    public void should_generate_sentence_when_words_given() {
        givenRandomWords(noun("apple"), verb("is"), adjective("green"));
        
        assertThat(sentenceService.generateSentence(), is(greaterThanOrEqualTo(0L)));
    }
    
    @Test
    public void should_get_previously_generated_sentence() {
        givenRandomWords(noun("apple"), verb("is"), adjective("green"));

        long sentenceId = sentenceService.generateSentence();
        assertThat(
                sentenceService.getSentence(sentenceId).get().getSentence(),
                is(new Sentence(noun("apple"), verb("is"), adjective("green"))));
    }
    
    @Test
    public void should_get_all_generated_sentences() {
        givenRandomWords(noun("apple"), verb("is"), adjective("green"));
        sentenceService.generateSentence();
        givenRandomWords(noun("snow"), verb("is"), adjective("white"));
        sentenceService.generateSentence();
        
        assertThat(
                sentenceService.getAllSentences().collect(toSet()),
                is(ImmutableSet.of(
                        new Sentence(noun("apple"), verb("is"), adjective("green")),
                        new Sentence(noun("snow"), verb("is"), adjective("white")))));
    }
    
    @Test
    public void should_get_sentence_with_info() {
        givenRandomWords(noun("apple"), verb("is"), adjective("green"));

        long sentenceId = sentenceService.generateSentence();

        SentenceWithInfo actualSentenceWithInfo = sentenceService.getSentence(sentenceId).get();
        assertThat(
                actualSentenceWithInfo.getSentence(),
                is(new Sentence(noun("apple"), verb("is"), adjective("green"))));
        assertThat(actualSentenceWithInfo.getGroupId(), is(1L));
    }

    @Test
    public void should_assign_different_groupIds_to_different_sentences() {
        givenRandomWords(noun("apple"), verb("is"), adjective("green"));
        long id1 = sentenceService.generateSentence();
        givenRandomWords(noun("apple"), verb("is"), adjective("red"));
        long id2 = sentenceService.generateSentence();

        SentenceWithInfo withInfo1 = sentenceService.getSentence(id1).get();
        SentenceWithInfo withInfo2 = sentenceService.getSentence(id2).get();
        assertThat(withInfo1.getGroupId(), is(not(equalTo(withInfo2.getGroupId()))));
    }

    @Test
    public void should_assign_same_groupId_to_same_sentences() {
        givenRandomWords(noun("apple"), verb("is"), adjective("green"));
        long id1 = sentenceService.generateSentence();
        long id2 = sentenceService.generateSentence();

        SentenceWithInfo withInfo1 = sentenceService.getSentence(id1).get();
        SentenceWithInfo withInfo2 = sentenceService.getSentence(id2).get();
        assertThat(withInfo1.getGroupId(), is(equalTo(withInfo2.getGroupId())));
    }

    @Test
    public void should_get_sentence_ids_for_group() {
        givenRandomWords(noun("apple"), verb("is"), adjective("green"));
        long id1 = sentenceService.generateSentence();
        long id2 = sentenceService.generateSentence();

        long groupId = sentenceService.getSentence(id1).get().getGroupId();
        
        assertThat(
                sentenceService.getSameSentenceGroup(groupId),
                is(Optional.of(new HashSet<>(asList(id1, id2)))));
    }

    
    private void givenRandomWords(Noun noun, Verb verb, Adjective adjective) {
        when(wordService.getRandomNoun()).thenReturn(Optional.ofNullable(noun));
        when(wordService.getRandomVerb()).thenReturn(Optional.ofNullable(verb));
        when(wordService.getRandomAdjective()).thenReturn(Optional.ofNullable(adjective));
    }
}
