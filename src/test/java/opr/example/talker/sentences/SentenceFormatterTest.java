package opr.example.talker.sentences;

import static opr.example.talker.words.Adjective.adjective;
import static opr.example.talker.words.Noun.noun;
import static opr.example.talker.words.Verb.verb;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class SentenceFormatterTest {

    private static final Sentence TEST_SENTENCE = new Sentence(noun("apple"), verb("is"), adjective("green"));
    
    @Test
    public void should_format_noun_verb_adjective_by_default() {
        assertThat(SentenceFormatter.DEFAULT.format(TEST_SENTENCE), is("apple is green"));
    }

    @Test
    public void should_format_adjective_noun_verb_when_yoda() {
        assertThat(SentenceFormatter.YODA_TALK.format(TEST_SENTENCE), is("green apple is"));
    }
}
