package opr.example.talker.json;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;

public class ParsingCheckerTest {

    private static final String PARSING_SUBJECT = "TestObject";
    
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    
    @Mock
    private JsonParser parser;
    private ParsingChecker checker;
    
    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        checker = new ParsingChecker(PARSING_SUBJECT, parser);
    }
    
    @Test
    public void should_return_when_predicate_true() throws JsonParseException {
        checker.checkJson(true);
    }

    @Test
    public void should_throw_JsonParseException_on_failure() throws JsonParseException {
        expectedException.expect(JsonParseException.class);
        checker.checkJson(false);
    }

    @Test
    public void should_throw_exception_with_default_message_on_failure() throws JsonParseException {
        expectedException.expectMessage("Failed to parse JSON for " + PARSING_SUBJECT);
        checker.checkJson(false);
    }

    @Test
    public void should_throw_exception_with_parser_attached() throws JsonParseException {
        try {
            checker.checkJson(false);
            fail("Expected JsonParseException");
        } catch (JsonParseException e) {
            assertThat(e.getProcessor(), is(parser));
        }
    }
}
