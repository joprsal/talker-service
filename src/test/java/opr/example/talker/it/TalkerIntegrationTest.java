package opr.example.talker.it;

import static net.javacrumbs.jsonunit.core.Option.IGNORING_EXTRA_FIELDS;
import static net.javacrumbs.jsonunit.core.Option.IGNORING_VALUES;
import static net.javacrumbs.jsonunit.fluent.JsonFluentAssert.assertThatJson;
import static opr.example.talker.words.Adjective.adjective;
import static opr.example.talker.words.Noun.noun;
import static opr.example.talker.words.Verb.verb;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import opr.example.talker.rest.sentences.SentencesHandler;
import opr.example.talker.rest.words.WordsHandler;
import opr.example.talker.sentences.SentenceService;
import opr.example.talker.words.Word;
import opr.example.talker.words.WordService;
import ratpack.http.client.ReceivedResponse;
import ratpack.test.embed.EmbeddedApp;
import ratpack.test.http.TestHttpClient;

/**
 * Collection of basic integration tests for the Talker service.
 */
public class TalkerIntegrationTest {

    private WordService wordService;
    private SentenceService sentenceService;
    private WordsHandler wordsHandler;
    private SentencesHandler sentencesHandler;

    private Set<String> blacklistedWords = new HashSet<>();
    @Mock
    private Random randomMock;
    private EmbeddedApp testApp;


	@Before
	public void init() {
	    MockitoAnnotations.initMocks(this);
	    setupRandomWordToBeAlwaysTheFirstInCategory();
	    
        wordService = new WordService(blacklistedWords, () -> new LinkedHashSet<>(), randomMock);
	    sentenceService = new SentenceService(wordService);
	    
        wordsHandler = new WordsHandler(wordService);
        sentencesHandler = new SentencesHandler(sentenceService);

		testApp = EmbeddedApp.fromHandlers(chain -> {
            wordsHandler.registerWith(chain);
            sentencesHandler.registerWith(chain);
		});
	}

    private void setupRandomWordToBeAlwaysTheFirstInCategory() {
        when(randomMock.nextInt(anyInt())).thenReturn(0);
    }


    @Test
    public void should_retrieve_all_added_words_in_alfabetical_order() throws Exception {
        testApp.test(httpClient -> {
            httpWordsPut(httpClient, noun("apple"), verb("is"), adjective("green"));
            assertThatJson(httpClient.getText("words"))
                    .isEqualTo("[\"apple\", \"green\", \"is\"]"); //alfabetical order
        });
    }

    @Test
    public void should_retrieve_individual_words_when_previously_added() throws Exception {
        testApp.test(httpClient -> {
            httpWordsPut(httpClient, noun("apple"), adjective("green"));
            assertThatJson(httpClient.getText("words/apple"))
                    .isEqualTo("{ \"word\": { \"word\": \"apple\", \"wordCategory\": \"NOUN\" } }");
            assertThatJson(httpClient.getText("words/is"))
                    .isEqualTo("{ \"word\": null }");
            assertThatJson(httpClient.getText("words/green"))
                    .isEqualTo("{ \"word\": { \"word\": \"green\", \"wordCategory\": \"ADJECTIVE\" } }");
        });
    }

    @Test
    public void should_generate_sentence() throws Exception {
        testApp.test(httpClient -> {
            httpWordsPut(httpClient, noun("apple"), verb("is"), adjective("green"));
            String newSentenceUri = httpSentencesGenerate(httpClient);
            assertThatJson(httpClient.getText(newSentenceUri))
                    .when(IGNORING_EXTRA_FIELDS)
                    .isEqualTo("{\"sentence\": { \"text\": \"apple is green\" } }");
        });
    }

    @Test
    public void should_generate_sentence_in_yoda_talk() throws Exception {
        testApp.test(httpClient -> {
            httpWordsPut(httpClient, noun("apple"), verb("is"), adjective("green"));
            String newSentenceUri = httpSentencesGenerate(httpClient);
            assertThatJson(httpClient.getText(newSentenceUri + "/yodaTalk"))
                    .when(IGNORING_EXTRA_FIELDS)
                    .isEqualTo("{\"sentence\": { \"text\": \"green apple is\"} }");
        });
    }

    @Test
    public void should_allow_generating_same_sentence_twice() throws Exception {
        testApp.test(httpClient -> {
            httpWordsPut(httpClient, noun("apple"), noun("pear"), verb("is"), adjective("green"));

            String newSentenceUri1 = httpSentencesGenerate(httpClient);
            assertThatJson(httpClient.getText(newSentenceUri1))
                    .when(IGNORING_EXTRA_FIELDS)
                    .isEqualTo("{\"sentence\": { \"text\": \"apple is green\" } }");
            
            String newSentenceUri2 = httpSentencesGenerate(httpClient);
            assertThatJson(httpClient.getText(newSentenceUri2))
                    .when(IGNORING_EXTRA_FIELDS)
                    .isEqualTo("{\"sentence\": { \"text\": \"apple is green\" } }");
            
            assertThat(newSentenceUri1, is(not(equalTo(newSentenceUri2))));
        });
    }

    @Test
    public void should_expose_sentence_group_stats() throws Exception {
        testApp.test(httpClient -> {
            httpWordsPut(httpClient, noun("apple"), verb("is"), adjective("green"));
            String newSentenceUri1 = httpSentencesGenerate(httpClient);
            String newSentenceUri2 = httpSentencesGenerate(httpClient);
            final String id1 = newSentenceUri1.split("/")[2];
            final String id2 = newSentenceUri2.split("/")[2];

            String sentenceJson = httpClient.getText(newSentenceUri1);
            assertThatJson(sentenceJson)
                    .when(IGNORING_EXTRA_FIELDS, IGNORING_VALUES)
                    .isEqualTo("{\"sentence\": { "
                            + "\"text\": \"green apple is\", "
                            + "\"sameSentenceGroupId\": 1 } }");
            Matcher matcher = Pattern.compile(".*\"sameSentenceGroupId\":\\s*(\\d+)\\s*.*").matcher(sentenceJson);
            assertThat(matcher.matches(), is(true));
            long sentenceGroupId = Long.parseLong(matcher.group(1));
            
            assertThatJson(httpClient.getText("sentences/sameSentenceGroup/" + sentenceGroupId))
                    .isEqualTo("{\"sameSentenceGroup\": { "
                            + "\"text\": \"apple is green\", "
                            + "\"count\": 2, "
                            + "\"sentenceIds\": [" + id1 + ", " + id2 + "] } }");
        });
    }

    @Test
    public void should_get_sentence_viewed_multiple_times() throws Exception {
        testApp.test(httpClient -> {
            httpWordsPut(httpClient, noun("apple"), verb("is"), adjective("green"));
            String sentenceUri = httpSentencesGenerate(httpClient);
            
            assertThatJson(httpClient.getText(sentenceUri)).node("sentence.viewDisplayCount").isEqualTo(1);
            assertThatJson(httpClient.getText(sentenceUri)).node("sentence.viewDisplayCount").isEqualTo(2);
            assertThatJson(httpClient.getText(sentenceUri)).node("sentence.viewDisplayCount").isEqualTo(3);
        });
    }

    
    private void httpWordsPut(TestHttpClient httpClient, Word... wordDefs) {
        for (Word wordDef : wordDefs) {
            httpClient.requestSpec(reqSpec -> {
                String wordCategory = wordDef.getCategory().name();
                reqSpec.getBody().text("{ \"word\": { \"wordCategory\": \"" + wordCategory + "\" } }");
            }).put("words/" + wordDef.getText());
        }
    }

    private String httpSentencesGenerate(TestHttpClient httpClient) {
        ReceivedResponse resp = httpClient.post("sentences/generate");
        assertThat(resp.getStatusCode(), is(201));
        return resp.getHeaders().get("Location");
    }
}
