package opr.example.talker.sentences;

public class SentenceWithInfo {

    private final Sentence sentence;
    private final long groupId;

    public SentenceWithInfo(Sentence sentence, long sameSentenceGroupId) {
        this.sentence = sentence;
        this.groupId = sameSentenceGroupId;
    }

    public Sentence getSentence() {
        return sentence;
    }

    public long getGroupId() {
        return groupId;
    }

    @Override
    public String toString() {
        return String.format("%s (gid=%d)", sentence, groupId);
    }
}
