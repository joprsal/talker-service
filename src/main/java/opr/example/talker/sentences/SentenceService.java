package opr.example.talker.sentences;

import static java.util.Collections.newSetFromMap;

import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Supplier;
import java.util.stream.Stream;

import opr.example.talker.words.Adjective;
import opr.example.talker.words.Noun;
import opr.example.talker.words.Verb;
import opr.example.talker.words.WordService;


public class SentenceService {

    private final WordService wordService;
    private final Map<Long, Sentence> id2sentence = new ConcurrentHashMap<>();
    private final Map<Sentence, Long> sentences2groupId = new ConcurrentHashMap<>();
    private final Map<Long, Set<Long>> groupId2sentenceIds = new ConcurrentHashMap<>();
    private final AtomicLong lastSentenceId = new AtomicLong(0L);
    private final AtomicLong lastGroupId = new AtomicLong(0L);

    public SentenceService(WordService wordService) {
        this.wordService = wordService;
    }

    public long generateSentence() {
        Noun noun = wordService.getRandomNoun().orElseThrow(illegalState("No nouns available"));
        Verb verb = wordService.getRandomVerb().orElseThrow(illegalState("No verbs available"));
        Adjective adjective = wordService.getRandomAdjective().orElseThrow(illegalState("No adjectives available"));
        
        Sentence sentence = new Sentence(noun, verb, adjective);

        long groupId = createMapping_sentence2groupId(sentence);
        long sentenceId = createMapping_id2sentence(sentence);
        updateMapping_groupId2sentenceIds(groupId, sentenceId);
        
        return sentenceId;
    }

    private long createMapping_sentence2groupId(Sentence sentence) {
        return sentences2groupId.computeIfAbsent(sentence, __ -> lastGroupId.incrementAndGet());
    }
    
    private long createMapping_id2sentence(Sentence sentence) {
        long id = lastSentenceId.incrementAndGet();
        id2sentence.put(id, sentence);
        return id;
    }

    private void updateMapping_groupId2sentenceIds(long groupId, long sentenceId) {
        Set<Long> sentenceIds = groupId2sentenceIds.computeIfAbsent(
                groupId,
                __ -> newSetFromMap(new ConcurrentHashMap<>()));
        sentenceIds.add(sentenceId);
    }

    public Optional<SentenceWithInfo> getSentence(long sentenceId) {
        Sentence sentence = id2sentence.get(sentenceId);
        if (sentence != null) {
            long groupId = sentences2groupId.get(sentence);
            return Optional.of(new SentenceWithInfo(sentence, groupId));
        } else {
            return Optional.empty();
        }
    }

    public Stream<Sentence> getAllSentences() {
        return id2sentence.values().stream();
    }

    /**
     * @return Returns sentence IDs belonging to the "same sentence" group.
     */
    public Optional<Set<Long>> getSameSentenceGroup(long groupId) {
        return Optional.ofNullable(groupId2sentenceIds.get(groupId));
    }

    
    private Supplier<? extends IllegalStateException> illegalState(String errMsg) {
        return () -> new IllegalStateException(errMsg);
    }
}
