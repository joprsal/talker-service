package opr.example.talker.sentences;

import java.util.function.Function;

public enum SentenceFormatter {

    DEFAULT(sentence -> String.format("%s %s %s",
            sentence.getNoun().getText(),
            sentence.getVerb().getText(),
            sentence.getAdjective().getText())),
    YODA_TALK(sentence -> String.format("%s %s %s",
            sentence.getAdjective().getText(),
            sentence.getNoun().getText(),
            sentence.getVerb().getText())),
    ;
    
    private final Function<Sentence, String> formatLogic;

    private SentenceFormatter(Function<Sentence, String> formatLogic) {
        this.formatLogic = formatLogic;
    }
    
    public String format(Sentence sentence) {
        return formatLogic.apply(sentence);
    }
}
