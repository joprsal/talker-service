package opr.example.talker.sentences;

import static com.google.common.base.Preconditions.checkNotNull;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import opr.example.talker.words.Adjective;
import opr.example.talker.words.Noun;
import opr.example.talker.words.Verb;

public class Sentence {

    private final Noun noun;
    private final Verb verb;
    private final Adjective adjective;

    public Sentence(Noun noun, Verb verb, Adjective adjective) {
        checkNotNull(noun, "Noun was null");
        checkNotNull(verb, "Verb was null");
        checkNotNull(adjective, "Adjective was null");
        this.noun = noun;
        this.verb = verb;
        this.adjective = adjective;
    }
    
    
    public Noun getNoun() {
        return noun;
    }

    public Verb getVerb() {
        return verb;
    }

    public Adjective getAdjective() {
        return adjective;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s", noun.getText(), verb.getText(), adjective.getText());
    }
    
    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(noun)
                .append(verb)
                .append(adjective)
                .toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (obj.getClass() != getClass()) {
            return false;
        }
        Sentence other = (Sentence) obj;
        return new EqualsBuilder()
                .append(noun, other.noun)
                .append(verb, other.verb)
                .append(adjective, other.adjective)
                .isEquals();
    }

}
