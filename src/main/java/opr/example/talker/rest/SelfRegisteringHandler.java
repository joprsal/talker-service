package opr.example.talker.rest;

import ratpack.handling.Chain;


public interface SelfRegisteringHandler {

	void registerWith(Chain chain);
}