package opr.example.talker.rest;

import java.io.IOException;
import java.util.Collections;
import java.util.Set;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import opr.example.talker.rest.sentences.SentencesHandler;
import opr.example.talker.rest.words.WordsHandler;
import opr.example.talker.sentences.SentenceService;
import opr.example.talker.words.WordService;
import ratpack.func.Action;
import ratpack.handling.Chain;
import ratpack.spring.config.EnableRatpack;


@SpringBootApplication
@EnableRatpack
public class RESTMain {

    private final SelfRegisteringHandler wordsHandler;
    private final SelfRegisteringHandler sentencesHandler;

	public RESTMain() throws IOException {
        WordService wordService = new WordService(readBlacklist());
        SentenceService sentenceService = new SentenceService(wordService);
        wordsHandler = new WordsHandler(wordService);
        sentencesHandler = new SentencesHandler(sentenceService);
    }

    private Set<String> readBlacklist() throws IOException {
        return Collections.emptySet();
    }

    
	@Bean
	public Action<Chain> root() {
		return chain -> {
			wordsHandler.registerWith(chain);
			sentencesHandler.registerWith(chain);
		};
	}

	public static void main(String[] args) {
		SpringApplication.run(RESTMain.class, args);
	}
}
