package opr.example.talker.rest.words;

import java.io.IOException;
import java.util.Optional;

import com.fasterxml.jackson.core.JsonGenerator;

import opr.example.talker.words.Word;
import opr.example.talker.words.WordService;


public class GetWordHandler {

    private final WordService wordService;

    public GetWordHandler(WordService wordService) {
        this.wordService = wordService;
    }

    public void handle(String wordInPath, JsonGenerator respGenerator) throws IOException {
        Optional<Word> word = wordService.getWord(wordInPath);
        respGenerator.writeStartObject();
        respGenerator.writeFieldName("word");
        writeWord(word, respGenerator);
        respGenerator.writeEndObject();
    }

    private void writeWord(Optional<Word> word, JsonGenerator generator) throws IOException {
        if (word.isPresent()) {
            generator.writeStartObject();
            generator.writeStringField("word", word.get().getText());
            generator.writeStringField("wordCategory", word.get().getCategory().name());
            generator.writeEndObject();
        } else {
            generator.writeNull();
        }
    }
}
