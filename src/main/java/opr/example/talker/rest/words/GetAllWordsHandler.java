package opr.example.talker.rest.words;

import java.io.IOException;
import java.util.Iterator;
import java.util.stream.Stream;

import com.fasterxml.jackson.core.JsonGenerator;

import opr.example.talker.words.Word;
import opr.example.talker.words.WordService;

public class GetAllWordsHandler {

    private final WordService wordService;

    public GetAllWordsHandler(WordService wordService) {
        this.wordService = wordService;
    }

    public void handle(JsonGenerator respGenerator) throws IOException {
        respGenerator.writeStartArray();
        writeAllWordsTo(respGenerator);
        respGenerator.writeEndArray();
    }

    private void writeAllWordsTo(JsonGenerator jsonGenerator) throws IOException {
        Stream<Word> allWords = wordService.getAllWords();
        Iterator<Word> iter = allWords.iterator();
        while (iter.hasNext()) {
            Word word = iter.next();
            jsonGenerator.writeString(word.getText());
        }
    }

}
