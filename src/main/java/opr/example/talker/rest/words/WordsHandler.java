package opr.example.talker.rest.words;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.google.common.base.Throwables;

import opr.example.talker.rest.SelfRegisteringHandler;
import opr.example.talker.words.WordService;
import ratpack.handling.Chain;
import ratpack.handling.Context;
import ratpack.http.internal.HttpHeaderConstants;


public class WordsHandler implements SelfRegisteringHandler {

    private final GetAllWordsHandler getAllWordsHandler;
    private final GetWordHandler getWordHandler;
    private final AddWordHandler addWordHandler;
    private final JsonFactory jsonFactory = new JsonFactory();

    public WordsHandler(WordService wordService) {
        this.getAllWordsHandler = new GetAllWordsHandler(wordService);
        this.getWordHandler = new GetWordHandler(wordService);
        this.addWordHandler = new AddWordHandler(wordService);
    }


    @Override
    public void registerWith(Chain chain) {
        try {
            chain.prefix("words", wordsChain -> {
                wordsChain.get(this::getAllWords);
                wordsChain.path(":word", ctx -> {
                    ctx.byMethod(m -> m
                            .get(this::getWord)
                            .put(this::putWord));
                });
            });
        } catch (Exception unexpected) {
            Throwables.propagate(unexpected);
        }
    }


    // GET all words
    private void getAllWords(Context ctx) throws IOException {
        String respText = generateJsonValue(jsonGenerator -> {
            getAllWordsHandler.handle(jsonGenerator);
        });
        ctx.getResponse()
                .contentTypeIfNotSet(HttpHeaderConstants.PLAIN_TEXT_UTF8)
                .send(respText);
    }

    // PUT word
    private void putWord(Context ctx) throws IOException {
        final String wordInPath = ctx.getPathTokens().get("word");
        ctx.getRequest().getBody().then(reqBody -> {
            JsonParser reqBodyParser = jsonFactory.createParser(reqBody.getInputStream());
            AtomicInteger statusCodeHolder = new AtomicInteger();
            addWordHandler.handle(wordInPath, reqBodyParser, statusCodeHolder);
            ctx.getResponse().getHeaders().add("Location", "/words/" + wordInPath);
            ctx.getResponse()
                    .status(statusCodeHolder.get())
                    .send();
        });
    }

    // GET word
    private void getWord(Context ctx) throws IOException {
        String wordInPath = ctx.getPathTokens().get("word");
        String respText = generateJsonValue(jsonGenerator -> {
            getWordHandler.handle(wordInPath, jsonGenerator);
        });
        ctx.getResponse()
                .contentTypeIfNotSet(HttpHeaderConstants.PLAIN_TEXT_UTF8)
                .send(respText);
    }


    private String generateJsonValue(JsonValueWriter jsonValueWriter) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (JsonGenerator jsonGenerator = jsonFactory.createGenerator(baos)) {
            jsonValueWriter.writeUsing(jsonGenerator);
        }
        return baos.toString();
    }

    private interface JsonValueWriter {
        void writeUsing(JsonGenerator jsonGenerator) throws IOException;
    }
}
