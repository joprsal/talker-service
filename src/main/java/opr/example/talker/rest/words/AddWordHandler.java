package opr.example.talker.rest.words;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;

import opr.example.talker.json.ParsingChecker;
import opr.example.talker.words.Word;
import opr.example.talker.words.WordCategory;
import opr.example.talker.words.WordService;

public class AddWordHandler {

    private final WordService wordService;

    public AddWordHandler(WordService wordService) {
        this.wordService = wordService;
    }

    public void handle(
            String wordInPath,
            JsonParser reqBodyParser,
            AtomicInteger statusCodeHolder) throws IOException {
        
        Word word = parseWord(wordInPath, reqBodyParser);
        boolean isNew = wordService.addWord(word);
        statusCodeHolder.set(isNew ? 201 : 200);
    }

    private Word parseWord(String wordText, JsonParser parser) throws IOException {
        ParsingChecker checker = new ParsingChecker("Word", parser);
        checker.checkJson(parser.nextToken().isStructStart());
        checker.checkJson("word".equals(parser.nextFieldName()));
        
        WordCategory category = parseWordCategory(parser);
        Word word = category.newWordOfThis(wordText);
        
        checker.checkJson(parser.nextToken().isStructEnd());
        return word;
    }

    private WordCategory parseWordCategory(JsonParser parser) throws JsonParseException, IOException {
        ParsingChecker checker = new ParsingChecker("Word category", parser);
        checker.checkJson(parser.nextToken().isStructStart());
        checker.checkJson("wordCategory".equals(parser.nextFieldName()));
        String categoryText = parser.nextTextValue();
        WordCategory category = WordCategory.valueOf(categoryText);
        checker.checkJson(parser.nextToken().isStructEnd());
        return category;
    }
}
