package opr.example.talker.rest.sentences;

import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import com.fasterxml.jackson.core.JsonGenerator;

import opr.example.talker.sentences.SentenceFormatter;
import opr.example.talker.sentences.SentenceService;
import opr.example.talker.sentences.SentenceWithInfo;

public class GetSentenceHandler {

    private final SentenceService sentenceService;
    private final ConcurrentHashMap<Long, AtomicLong> sentenceId2displayCount = new ConcurrentHashMap<>();

    public GetSentenceHandler(SentenceService sentenceService) {
        this.sentenceService = sentenceService;
    }

    public void handle(long sentenceId, SentenceFormatter formatter, JsonGenerator respGenerator) throws IOException {
        Optional<SentenceWithInfo> sentence = sentenceService.getSentence(sentenceId);
        respGenerator.writeStartObject();
        respGenerator.writeFieldName("sentence");
        writeSentence(sentenceId, sentence, formatter, respGenerator);
        respGenerator.writeEndObject();
    }

    private void writeSentence(
            long sentenceId,
            Optional<SentenceWithInfo> sentenceOpt,
            SentenceFormatter formatter,
            JsonGenerator generator) throws IOException {
        
        if (sentenceOpt.isPresent()) {
            final SentenceWithInfo sentence = sentenceOpt.get();
            generator.writeStartObject();
            generator.writeStringField("text", formatter.format(sentence.getSentence()));
            generator.writeNumberField("viewDisplayCount", increaseAndGetDisplayCount(sentenceId));
            generator.writeNumberField("sameSentenceGroupId", sentence.getGroupId());
            generator.writeEndObject();
        } else {
            generator.writeNull();
        }
    }

    private long increaseAndGetDisplayCount(long sentenceId) {
        AtomicLong sentenceDisplayCount =
                sentenceId2displayCount.computeIfAbsent(sentenceId, __ -> new AtomicLong());
        return sentenceDisplayCount.incrementAndGet();
    }

}
