package opr.example.talker.rest.sentences;

import java.io.IOException;
import java.util.Optional;
import java.util.Set;

import com.fasterxml.jackson.core.JsonGenerator;

import opr.example.talker.sentences.Sentence;
import opr.example.talker.sentences.SentenceFormatter;
import opr.example.talker.sentences.SentenceService;

public class GetSameSentenceGroupHandler {

    private final SentenceService sentenceService;

    public GetSameSentenceGroupHandler(SentenceService sentenceService) {
        this.sentenceService = sentenceService;
    }

    public void handle(long sameSentenceGroupId, JsonGenerator respGenerator) throws IOException {
        Optional<Set<Long>> sentenceIdsOpt = sentenceService.getSameSentenceGroup(sameSentenceGroupId);
        respGenerator.writeStartObject();
        respGenerator.writeFieldName("sameSentenceGroup");
        writeSameSentenceGroup(sentenceIdsOpt, respGenerator);
        respGenerator.writeEndObject();
    }

    private void writeSameSentenceGroup(
            Optional<Set<Long>> sentenceIdsOpt,
            JsonGenerator respGenerator) throws IOException {
        
        if (sentenceIdsOpt.isPresent()) {
            respGenerator.writeStartObject();
            long[] sentenceIds = sentenceIdsOpt.get().stream().mapToLong(l -> l).toArray();
            respGenerator.writeFieldName("text");
            respGenerator.writeString(fetchSampleSentence(sentenceIds[0]));
            respGenerator.writeFieldName("count");
            respGenerator.writeNumber(sentenceIds.length);
            respGenerator.writeFieldName("sentenceIds");
            respGenerator.writeArray(sentenceIds, 0, sentenceIds.length);
            respGenerator.writeEndObject();
        } else {
            respGenerator.writeNull();
        }
    }

    private String fetchSampleSentence(long sentenceId) {
        Sentence sentence = sentenceService.getSentence(sentenceId).get().getSentence();
        return SentenceFormatter.DEFAULT.format(sentence);
    }
}
