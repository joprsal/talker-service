package opr.example.talker.rest.sentences;

import java.util.concurrent.atomic.AtomicLong;

import opr.example.talker.sentences.SentenceService;

public class GenerateSentenceHandler {

    private final SentenceService sentenceService;

    public GenerateSentenceHandler(SentenceService sentenceService) {
        this.sentenceService = sentenceService;
    }

    public void handle(AtomicLong sentenceIdHolder) {
        long id = sentenceService.generateSentence();
        sentenceIdHolder.set(id);
    }
}
