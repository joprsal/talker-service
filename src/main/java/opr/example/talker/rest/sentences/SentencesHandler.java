package opr.example.talker.rest.sentences;

import static opr.example.talker.sentences.SentenceFormatter.DEFAULT;
import static opr.example.talker.sentences.SentenceFormatter.YODA_TALK;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.google.common.base.Throwables;

import opr.example.talker.rest.SelfRegisteringHandler;
import opr.example.talker.sentences.SentenceFormatter;
import opr.example.talker.sentences.SentenceService;
import ratpack.handling.Chain;
import ratpack.handling.Context;
import ratpack.http.internal.HttpHeaderConstants;

public class SentencesHandler implements SelfRegisteringHandler {

    private final JsonFactory jsonFactory = new JsonFactory();
    private final GetAllSentencesHandler getAllSentencesHandler;
    private final GenerateSentenceHandler generateSentenceHandler;
    private final GetSentenceHandler getSentenceHandler;
    private final GetSameSentenceGroupHandler getSameSentenceGroupHandler;

    public SentencesHandler(SentenceService sentenceService) {
        this.getAllSentencesHandler = new GetAllSentencesHandler(sentenceService);
        this.generateSentenceHandler = new GenerateSentenceHandler(sentenceService);
        this.getSentenceHandler = new GetSentenceHandler(sentenceService);
        this.getSameSentenceGroupHandler = new GetSameSentenceGroupHandler(sentenceService);
    }

    @Override
    public void registerWith(Chain chain) {
        try {
            chain.prefix("sentences", sentencesChain -> {
                sentencesChain
                        .get(this::getAllSentences)
                        .get(":sentenceId:\\d+/:dialect?:yodaTalk", this::getSentence)
                        .get("sameSentenceGroup/:groupId:\\d+", this::getSameSentenceGroup)
                        .post("generate", this::generateSentence);
            });
        } catch (Exception unexpected) {
            Throwables.propagate(unexpected);
        }
    }

    // GET all sentences
    private void getAllSentences(Context ctx) throws IOException {
        String respText = generateJsonValue(jsonGenerator -> {
            getAllSentencesHandler.handle(jsonGenerator);
        });
        ctx.getResponse()
                .contentTypeIfNotSet(HttpHeaderConstants.PLAIN_TEXT_UTF8)
                .send(respText);
    }
    
    // POST generate sentence
    private void generateSentence(Context ctx) throws IOException {
        AtomicLong sentenceIdHolder = new AtomicLong();
        generateSentenceHandler.handle(sentenceIdHolder);
        ctx.getResponse().getHeaders().add("Location", "/sentences/"+sentenceIdHolder.get());
        ctx.getResponse()
                .status(201)
                .send();
    }
    
    // GET sentence by id
    private void getSentence(Context ctx) throws IOException {
        String sentenceIdStr = ctx.getPathTokens().get("sentenceId");
        final long sentenceId = Long.parseLong(sentenceIdStr);
        String dialect = ctx.getPathTokens().get("dialect");
        final SentenceFormatter sentenceFormatter = "yodaTalk".equals(dialect) ? YODA_TALK : DEFAULT;
        
        String respText = generateJsonValue(jsonGenerator -> {
            getSentenceHandler.handle(sentenceId, sentenceFormatter, jsonGenerator);
        });
        ctx.getResponse()
                .contentTypeIfNotSet(HttpHeaderConstants.PLAIN_TEXT_UTF8)
                .send(respText);
    }
    
    // GET same sentence group by groupId
    private void getSameSentenceGroup(Context ctx) throws IOException {
        String groupIdStr = ctx.getPathTokens().get("groupId");
        final long groupId = Long.parseLong(groupIdStr);
        
        String respText = generateJsonValue(jsonGenerator -> {
            getSameSentenceGroupHandler.handle(groupId, jsonGenerator);
        });
        ctx.getResponse()
                .contentTypeIfNotSet(HttpHeaderConstants.PLAIN_TEXT_UTF8)
                .send(respText);
    }

    
    private String generateJsonValue(JsonValueWriter jsonValueWriter) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (JsonGenerator jsonGenerator = jsonFactory.createGenerator(baos)) {
            jsonValueWriter.writeUsing(jsonGenerator);
        }
        return baos.toString();
    }

    private interface JsonValueWriter {
        void writeUsing(JsonGenerator jsonGenerator) throws IOException;
    }
}
