package opr.example.talker.rest.sentences;

import java.io.IOException;
import java.util.Iterator;
import java.util.stream.Stream;

import com.fasterxml.jackson.core.JsonGenerator;

import opr.example.talker.sentences.Sentence;
import opr.example.talker.sentences.SentenceFormatter;
import opr.example.talker.sentences.SentenceService;

public class GetAllSentencesHandler {

    private final SentenceService sentenceService;

    public GetAllSentencesHandler(SentenceService sentenceService) {
        this.sentenceService = sentenceService;
    }

    public void handle(JsonGenerator respGenerator) throws IOException {
        respGenerator.writeStartArray();
        writeAllSentencesTo(respGenerator);
        respGenerator.writeEndArray();
    }

    private void writeAllSentencesTo(JsonGenerator jsonGenerator) throws IOException {
        Stream<Sentence> all = sentenceService.getAllSentences();
        Iterator<Sentence> iter = all.iterator();
        while (iter.hasNext()) {
            Sentence sentence = iter.next();
            jsonGenerator.writeString(SentenceFormatter.DEFAULT.format(sentence));
        }
    }

}
