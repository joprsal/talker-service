package opr.example.talker.json;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;

public class ParsingChecker {

    private final JsonParser parser;
    private final String errMsg;
    
    public ParsingChecker(String parsingSubject, JsonParser parser) {
        this.parser = parser;
        this.errMsg = "Failed to parse JSON for " + parsingSubject;
    }

    public void checkJson(boolean predicate) throws JsonParseException {
        if (!predicate) {
            throw new JsonParseException(parser, errMsg);
        }
    }

}
