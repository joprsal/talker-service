package opr.example.talker.words;

public class Verb extends Word {

    public Verb(String text) {
        super(text);
    }

    public static Verb verb(String text) {
        return new Verb(text);
    }

    @Override
    public WordCategory getCategory() {
        return WordCategory.VERB;
    }

    @Override
    public <T> T byCategory(
            LogicForCategory<Noun, T> logicForNoun,
            LogicForCategory<Verb, T> logicForVerb,
            LogicForCategory<Adjective, T> logicForAdjective) {

        return logicForVerb.call(this);
    }
}
