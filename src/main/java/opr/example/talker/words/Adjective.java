package opr.example.talker.words;

public class Adjective extends Word {

    public Adjective(String text) {
        super(text);
    }

    public static Adjective adjective(String text) {
        return new Adjective(text);
    }

    @Override
    public WordCategory getCategory() {
        return WordCategory.ADJECTIVE;
    }

    @Override
    public <T> T byCategory(
            LogicForCategory<Noun, T> logicForNoun,
            LogicForCategory<Verb, T> logicForVerb,
            LogicForCategory<Adjective, T> logicForAdjective) {

        return logicForAdjective.call(this);
    }
}
