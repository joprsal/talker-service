package opr.example.talker.words;

import java.util.function.Function;

public enum WordCategory {

	NOUN(text -> new Noun(text)),
	VERB(text -> new Verb(text)),
	ADJECTIVE(text -> new Adjective(text)),
	;
    
    private final Function<String, Word> wordCreateLogic;
    
    private WordCategory(Function<String, Word> wordCreateLogic) {
        this.wordCreateLogic = wordCreateLogic;
    }
    
    /**
     * Creates a new word of this category.
     */
    public Word newWordOfThis(String text) {
        return wordCreateLogic.apply(text);
    }
}
