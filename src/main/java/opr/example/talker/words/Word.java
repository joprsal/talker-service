package opr.example.talker.words;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang.StringUtils.isBlank;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public abstract class Word {
	
	private final String text;
	
    Word(String text) {
        checkNotNull(text, "Word text cannot be null");
        checkArgument(!isBlank(text), "Word text cannot be blank");
        this.text = text;
    }


    public abstract WordCategory getCategory();

    public abstract <T> T byCategory(
            LogicForCategory<Noun, T> logicForNoun,
            LogicForCategory<Verb, T> logicForVerb,
            LogicForCategory<Adjective, T> logicForAdjective);

	public String getText() {
		return text;
	}
    
	@Override
	public String toString() {
	    return String.format("%s (%s)", text, getCategory());
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder()
				.append(text)
				.append(getCategory())
				.toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
	   if (obj == null) { return false; }
	   if (obj == this) { return true; }
	   if (obj.getClass() != getClass()) {
	       return false;
	   }
	   Word other = (Word) obj;
	   return new EqualsBuilder()
			.append(text, other.text)
			.append(getCategory(), other.getCategory())
			.isEquals();
	}

	
    @FunctionalInterface
    public interface LogicForCategory<P, R> {
        R call(P wordOfThisCategory);
    }
}
