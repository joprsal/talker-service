package opr.example.talker.words;

@SuppressWarnings("serial")
public class IllegalInputException extends RuntimeException {

    public IllegalInputException(String msg) {
        super(msg);
    }
}
