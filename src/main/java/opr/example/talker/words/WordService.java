package opr.example.talker.words;

import static java.util.Collections.newSetFromMap;

import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;
import java.util.stream.Stream;

import com.google.common.annotations.VisibleForTesting;

public class WordService {
    
    private final Set<String> blacklistedWords;
    private final Set<Noun> nouns;
    private final Set<Verb> verbs;
    private final Set<Adjective> adjectives;
    private final Random random;
	
    public WordService(Set<String> blacklistedWords) {
        this(
                blacklistedWords,
                () -> newSetFromMap(new ConcurrentHashMap<>()),
                new Random());
    }

    @VisibleForTesting
    @SuppressWarnings("unchecked")
    public WordService(
            Set<String> blacklistedWords,
            Supplier<Set<? extends Word>> wordSetFactory,
            Random random) {
        this.blacklistedWords = blacklistedWords;
        this.nouns = (Set<Noun>) wordSetFactory.get();
        this.verbs = (Set<Verb>) wordSetFactory.get();
        this.adjectives = (Set<Adjective>) wordSetFactory.get();
        this.random = random;
    }
    
    /**
     * @return Returns whether the given word was newly added.
     */
	public boolean addWord(Word word) {
	    checkWord(word);
	    return word.byCategory(
	            noun -> nouns.add(noun),
	            verb -> verbs.add(verb),
	            adjective -> adjectives.add(adjective));
	}

    private void checkWord(Word word) {
        if (blacklistedWords.contains(word.getText())) {
            throw new IllegalInputException("Given word is not allowed: " + word.getText());
        }
    }

    //TODO what if two words have same text? e.g. move(NOUN) and move(VERB)
    public Optional<Word> getWord(String searchWord) {
        return allWords()
                .filter(word -> word.getText().equals(searchWord))
                .findFirst();
    }
    
    public Stream<Word> getAllWords() {
        return allWords().sorted((w1, w2) -> w1.getText().compareTo(w2.getText()));
    }

    private Stream<Word> allWords() {
        return Stream.of(nouns, verbs, adjectives)
                .flatMap(wordSet -> wordSet.stream())
                .map(Word.class::cast);
    }

    
    public Optional<Noun> getRandomNoun() {
        return getRandomWordFrom(nouns);
    }

    public Optional<Verb> getRandomVerb() {
        return getRandomWordFrom(verbs);
    }

    public Optional<Adjective> getRandomAdjective() {
        return getRandomWordFrom(adjectives);
    }

    private <T extends Word> Optional<T> getRandomWordFrom(Set<T> words) {
        if (words.isEmpty()) {
            return Optional.empty();
        } else {
            int skipCount = random.nextInt(words.size());
            return words.stream().skip(skipCount).findFirst();
        }
    }
}
