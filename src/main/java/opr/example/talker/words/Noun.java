package opr.example.talker.words;

public class Noun extends Word {

    public Noun(String text) {
        super(text);
    }

    public static Noun noun(String text) {
        return new Noun(text);
    }

    @Override
    public WordCategory getCategory() {
        return WordCategory.NOUN;
    }

    @Override
    public <T> T byCategory(
            LogicForCategory<Noun, T> logicForNoun,
            LogicForCategory<Verb, T> logicForVerb,
            LogicForCategory<Adjective, T> logicForAdjective) {

        return logicForNoun.call(this);
    }
}
